import socket
import subprocess

from flask import Flask

app = Flask(__name__)


@app.get('/')
def get_private_ip():
    return socket.gethostname()


@app.post('/')
def stress_cpu():
    return subprocess.Popen(['python', 'stress_cpu.py'])


if __name__ == '__main__':
    app.run(debug=True, port=8080)
